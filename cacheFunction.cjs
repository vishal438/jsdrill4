function cacheFunction(callback){
   if(typeof callback !=="function"){
      throw new Error("function not defined")
   }
const cache={}
return function(...args){
   
   const key=JSON.stringify(args)
   if (key in cache){
    return cache[key]
   }
   const result=callback(...args)
   cache[key]=result;
   return result
}

}
module.exports=cacheFunction