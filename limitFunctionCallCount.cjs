function limitFunctionCallCount(callback,n){
    let counter=0;
    if(typeof callback !=="function"){
        throw new Error("parameters must be function")
    }
    if(typeof n!=="number" || n<=0){
        throw new Error("the second parameter must be positive")
    }
 return function(){

    if(counter<n){
        counter++
        return callback.apply(null,arguments)
    }
    else{
        return null
    }
 }

}
module.exports=limitFunctionCallCount;